import {
  Montserrat_400Regular,
  Montserrat_500Medium,
  Montserrat_600SemiBold,
  Montserrat_700Bold,
  useFonts,
} from "@expo-google-fonts/montserrat";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { StyleSheet, Text } from "react-native";
import colors from "./assets/colors/color";
import Details from "./screens/Details";
import Home from "./screens/Home";
import { RootStackParamList } from "./screens/RootStackParam";

export default function App() {
  let [fontsLoaded] = useFonts({
    Montserrat_400Regular,
    Montserrat_600SemiBold,
    Montserrat_500Medium,
    Montserrat_700Bold,
  });

  const RootStack = createNativeStackNavigator<RootStackParamList>();
  if (!fontsLoaded) {
    return <Text>Loading...</Text>;
  } else
    return (
      <NavigationContainer>
        <RootStack.Navigator initialRouteName="Home">
          <RootStack.Screen
            name="Home"
            component={Home}
            options={{ headerShown: false }}
          />
          <RootStack.Screen
            name="Details"
            component={Details}
            options={{ headerShown: false }}
          />
        </RootStack.Navigator>
      </NavigationContainer>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  HIHI: {
    fontFamily: "Montserrat_400Regular",
    color: colors.price,
  },
});
