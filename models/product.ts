import { ImageSourcePropType } from "react-native";

export interface Product{
    id : number;
    image:ImageSourcePropType;
    title:string;
    selected:boolean;
}