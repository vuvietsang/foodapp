import { ImageSourcePropType } from "react-native";

export default interface Popular{
    id:number;
    image:ImageSourcePropType;
    title:string;
    weight:string;
    rating:string;
    price:number,
    sizename:string,
    sizeNumber:number,
    crust:string,
    deliveryTime:number,
    ingredients:Array<{
        id:number,
        name:string,
        image:any,
    }>

    
}