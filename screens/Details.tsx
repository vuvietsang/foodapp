import { Feather, MaterialCommunityIcons } from "@expo/vector-icons";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import * as React from "react";
import {
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { TouchableOpacity } from "react-native";
import colors from "../assets/colors/color";
import popularData from "../assets/data/popularData";
import Popular from "../models/popular";
import { RootStackParamList } from "./RootStackParam";

type Props = NativeStackScreenProps<RootStackParamList, "Details">;
const Details = ({ navigation, route }: Props) => {
  const item: Popular = route.params?.item;
  const renderIngredientItems = ({ item }: any) => {
    return (
      <View style={styles.ingredientItemWrapper}>
        <Image style={styles.imgIngredient} source={item.image} />
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <SafeAreaView>
        <View style={styles.headerWrapper}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
          >
            <View style={styles.headerLeft}>
              <Feather name="chevron-left" size={12} color="#000" />
            </View>
          </TouchableOpacity>
          <View style={styles.headerRight}>
            <MaterialCommunityIcons
              name="star"
              size={12}
              color={colors.white}
            />
          </View>
        </View>
      </SafeAreaView>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentInsetAdjustmentBehavior="automatic"
      >
        {/* Title */}
        <View style={styles.titleWrapper}>
          <Text style={styles.title}>{item.title}</Text>
        </View>
        {/* Price */}
        <View style={styles.priceWrapper}>
          <Text style={styles.price}>${item.price}</Text>
        </View>
        {/* Pizza info */}
        <View style={styles.infoWrapper}>
          <View>
            <View style={styles.infoItemWrapper}>
              <Text style={styles.info}>Size</Text>
              <Text style={styles.value}>
                {item.sizename} {item.sizeNumber}
              </Text>
            </View>
            <View style={styles.infoItemWrapper}>
              <Text style={styles.info}>Crust</Text>
              <Text style={styles.value}>{item.crust}</Text>
            </View>
            <View style={styles.infoItemWrapper}>
              <Text style={styles.info}>Delivery in</Text>
              <Text style={styles.value}>{item.deliveryTime} mins</Text>
            </View>
          </View>
          <View style={styles.infoRight}>
            <Image source={item.image} />
          </View>
        </View>
        {/* Ingredient */}
        <View style={styles.ingredientWrapper}>
          <Text style={styles.ingredientTitle}>Ingredients</Text>
          <View style={styles.ingredientListWrapper}>
            <FlatList
              data={item.ingredients}
              keyExtractor={(item) => item.id.toString()}
              renderItem={renderIngredientItems}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            />
          </View>
        </View>
        {/* Place an order */}
        <SafeAreaView>
          <TouchableOpacity
            onPress={() => {
              alert("Your order has been placed!");
            }}
          >
            <View style={styles.orderWrapper}>
              <Text style={styles.orderText}>Place an order</Text>
              <Feather name="chevron-right" size={18} color={colors.textDark} />
            </View>
          </TouchableOpacity>
        </SafeAreaView>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingTop: 20,
    paddingHorizontal: 20,
  },
  headerLeft: {
    borderColor: colors.textLight,
    borderWidth: 2,
    borderRadius: 10,
    padding: 12,
  },
  headerRight: {
    backgroundColor: colors.primary,
    padding: 12,
    borderWidth: 2,
    borderColor: colors.primary,
    borderRadius: 10,
  },
  titleWrapper: {
    marginTop: 30,
    marginHorizontal: 20,
  },
  title: {
    fontFamily: "Montserrat_700Bold",
    fontSize: 32,
    color: colors.textDark,
    width: "55%",
  },
  priceWrapper: {
    marginHorizontal: 20,
    marginTop: 20,
  },
  price: {
    fontFamily: "Montserrat_600SemiBold",
    fontSize: 32,
    color: colors.secondary,
  },
  infoWrapper: {
    marginHorizontal: 20,
    marginTop: 30,
    flexDirection: "row",
    alignItems: "center",
  },
  infoRight: {
    marginLeft: 40,
  },
  info: {
    color: colors.textLight,
    fontFamily: "Montserrat_500Medium",
  },
  value: {
    color: colors.textDark,
    fontFamily: "Montserrat_600SemiBold",
  },
  infoItemWrapper: {
    marginBottom: 20,
  },
  ingredientWrapper: {
    marginTop: 40,
    marginHorizontal: 20,
  },
  ingredientTitle: {
    fontFamily: "Montserrat_700Bold",
    fontSize: 16,
  },
  ingredientListWrapper: {
    marginTop: 20,
  },
  ingredientItemWrapper: {
    backgroundColor: "#FFFFFF",
    marginRight: 15,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.05,
    shadowRadius: 10,
    elevation: 2,
  },
  imgIngredient: {
    resizeMode: "contain",
  },
  orderWrapper: {
    marginTop: 60,
    marginHorizontal: 20,
    backgroundColor: colors.primary,
    borderRadius: 50,
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
  },
  orderText: {
    paddingVertical: 23,
    fontFamily: "Montserrat_700Bold",
    paddingRight: 11,
  },
});

export default Details;
