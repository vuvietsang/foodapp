import { Entypo, Feather, MaterialCommunityIcons } from "@expo/vector-icons";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import * as React from "react";
import { Platform } from "react-native";

import {
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import colors from "../assets/colors/color";
import categoriesData from "../assets/data/categoriesData";
import popularData from "../assets/data/popularData";
import { RootStackParamList } from "./RootStackParam";

type Props = NativeStackScreenProps<RootStackParamList, "Home">;

const Home = ({ route, navigation }: Props) => {
  const renderCategoriesItem = ({ item }: any) => {
    return (
      <View
        style={[
          styles.categoriesItemWrapper,
          {
            backgroundColor: item.selected ? colors.primary : colors.white,
            marginLeft: item.id !== 1 ? 20 : 0,
          },
        ]}
      >
        <Image source={item.image} style={styles.categoriesItemImage} />
        <Text style={styles.categoriesItemTitle}>{item.title}</Text>
        <View
          style={[
            styles.categoriesSelectWrapper,
            {
              backgroundColor: item.selected ? colors.white : colors.secondary,
            },
          ]}
        >
          <Feather
            name="chevron-right"
            size={9}
            style={[styles.categoriesItemIcon]}
            color={item.selected ? "#000000" : colors.white}
          />
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentInsetAdjustmentBehavior="automatic"
      >
        <SafeAreaView>
          {Platform.OS == "ios" ? (
            <View style={styles.headerWrapper}>
              <Image
                source={require("../assets/images/profile.png")}
                style={styles.profileImg}
              />
              <Feather name="menu" size={24} color={colors.textDark} />
            </View>
          ) : (
            <View style={[styles.headerWrapper, { marginTop: 30 }]}>
              <Image
                source={require("../assets/images/profile.png")}
                style={styles.profileImg}
              />
              <Feather name="menu" size={24} color={colors.textDark} />
            </View>
          )}
        </SafeAreaView>

        {/* title */}
        <View style={styles.titleWrapper}>
          <Text style={styles.titleSubTitle}>Food</Text>
          <Text style={styles.titlesTitle}>Delivery</Text>
        </View>
        {/* Search */}
        <View style={styles.searchWrapper}>
          <Feather name="search" size={16} color={colors.textDark} />
          <View style={styles.search}>
            <Text style={styles.searchText}>Search...</Text>
          </View>
        </View>

        {/* Categories */}
        <View style={styles.categoriesWrapper}>
          <Text style={styles.categoriesTitle}>Categories</Text>
          <View style={styles.listCategories}>
            <FlatList
              data={categoriesData}
              keyExtractor={(item) => item.id.toString()}
              renderItem={renderCategoriesItem}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            />
          </View>
        </View>
        {/* Popular */}
        <View style={styles.popularWrapper}>
          <Text style={styles.popularTitle}>Popular</Text>
          <View style={styles.popularWrapperList}>
            {popularData.map((item) => (
              <TouchableOpacity
                key={item.id}
                onPress={() => {
                  navigation.navigate("Details", { item: item });
                }}
              >
                <View style={styles.popularItem}>
                  {/* POPULAR INFO */}
                  <View style={styles.popularIteminfo}>
                    <View style={styles.cateforiesInfoWrapper}>
                      <View style={styles.popularItemTitleWrapper}>
                        <MaterialCommunityIcons
                          name="crown"
                          size={12}
                          color={colors.primary}
                        />
                        <Text style={styles.popularItemTitle}>
                          top of the week{" "}
                        </Text>
                      </View>
                      <Text style={styles.categoriesName}>{item.title}</Text>
                      <Text style={styles.categoriesWeight}>
                        Weight {item.weight}
                      </Text>
                    </View>
                    <View style={styles.categoriesImgWrapper}>
                      <Image style={styles.categoriesImg} source={item.image} />
                    </View>
                  </View>
                  {/* ACTION WRAPPER */}
                  <View style={styles.actionWrapper}>
                    <View style={styles.plusWrapper}>
                      <Entypo name="plus" size={10} color="black" />
                    </View>
                    <View style={styles.rating}>
                      <Entypo name="star" size={10} color="black" />
                      <Text style={{ marginHorizontal: 5 }}>5.0</Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    paddingTop: 20,
    alignItems: "center",
  },
  profileImg: {
    width: 40,
    height: 40,
    borderRadius: 100,
  },
  titleSubTitle: {
    fontFamily: "Montserrat_400Regular",
    fontSize: 16,
    color: colors.textDark,
  },
  titleWrapper: {
    marginTop: 30,
    marginHorizontal: 20,
  },
  titlesTitle: {
    marginTop: 5,
    fontFamily: "Montserrat_700Bold",
    fontSize: 32,
    color: colors.textDark,
  },
  searchWrapper: {
    marginTop: 36,
    marginHorizontal: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  search: {
    flex: 1,
    marginLeft: 10,
    borderBottomColor: colors.textLight,
    borderBottomWidth: 2,
  },
  searchText: {
    fontFamily: "Montserrat_600SemiBold",
    color: colors.textLight,
    fontSize: 14,
    marginBottom: 4.97,
  },
  categoriesWrapper: {
    marginTop: 30,
    marginHorizontal: 20,
  },
  listCategories: {
    marginTop: 15,
    paddingBottom: 20,
  },
  categoriesTitle: {
    fontFamily: "Montserrat_700Bold",
    fontSize: 16,
  },
  categoriesItemWrapper: {
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.05,
    shadowRadius: 10,
    elevation: 2,
  },
  categoriesItemImage: {
    alignSelf: "center",
    width: 60,
    height: 60,
    marginTop: 24,
    marginHorizontal: 20,
  },
  categoriesItemTitle: {
    marginTop: 10,
    textAlign: "center",
    fontFamily: "Montserrat_600SemiBold",
  },
  categoriesSelectWrapper: {
    marginTop: 20,
    width: 26,
    height: 26,
    borderRadius: 100,
    backgroundColor: colors.background,
    alignSelf: "center",
    justifyContent: "center",
    marginBottom: 20,
  },
  categoriesItemIcon: {
    alignSelf: "center",
  },
  popularWrapper: {
    marginHorizontal: 20,
  },
  popularTitle: {
    fontFamily: "Montserrat_700Bold",
    fontSize: 16,
  },
  popularWrapperList: {
    marginTop: 11,
  },
  popularItem: {
    backgroundColor: "white",
    marginBottom: 20,
    borderRadius: 25,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.05,
    shadowRadius: 10,
    elevation: 2,
  },
  popularIteminfo: {
    flexDirection: "row",
  },
  popularItemTitleWrapper: {
    paddingTop: 28,
    flexDirection: "row",
    alignItems: "center",
  },
  popularItemTitle: {
    paddingHorizontal: 10,
  },
  cateforiesInfoWrapper: {
    marginHorizontal: 20,
  },
  categoriesName: {
    fontFamily: "Montserrat_600SemiBold",
    marginTop: 20,
  },
  categoriesWeight: {
    marginTop: 5,
    fontSize: 12,
    fontFamily: "Montserrat_500Medium",
    color: colors.textLight,
  },
  categoriesImg: {
    width: 210,
    height: 125,
    position: "absolute",
  },
  categoriesImgWrapper: {
    marginTop: 18,
  },
  actionWrapper: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 10,
  },
  plusWrapper: {
    backgroundColor: colors.primary,
    width: 90,
    height: 53,
    borderBottomLeftRadius: 25,
    borderTopRightRadius: 25,
    justifyContent: "center",
    alignItems: "center",
  },
  rating: {
    flexDirection: "row",
    marginHorizontal: 20,
    alignItems: "center",
  },
});
export default Home;
